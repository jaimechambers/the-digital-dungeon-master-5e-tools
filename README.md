# The Digital Dungeon Master 5e Tools

A collection of D&D 5th Edition specific macros, roll tables, journals, scenes, and more for use in Foundry VTT.

[[_TOC_]]

## Installation

To install, follow these instructions:

1. Open your instance of **Foundry VTT** and login
2. From within the **Foundry Virtual Tabletop - Configuration and Setup**, click on the **Add-on Modules** tab
3. Click on the **Install Module** button in the bottom left-hand corner
4. Paste the URL provided below into the **Manifest URL**: box
5. Click the **Install** button

**Manifest URL:** https://gitlab.com/jaimechambers/the-digital-dungeon-master-5e-tools/-/raw/main/module.json

## What Does It Do?

This add-on module will install a number of compendium files that provide you the ability to easily import macros, roll tables, journal entries, scenes, and more. A full list of the included items is provided below. 

## Usage

Once you have installed the module, you will find a number of new "TDDM - XXXXXX" compendiums added to your Compendium Packs tab within Foundry VTT. You can view the contents of the compendiums by clicking on them. You can also import the contents to your Foundry VTT campaign. 

**Please note**, the compendiums represent many different types of Foundry VTT materials. You can import Items, Macros, Scenes, and Roll Tables. These items, when imported, can be found under their respective tabs within your campaign. 

## Included Compendiums

-TBD 

## Bug Reporting

**Bug Reporter:** This module supports and utilizes the Bug Reporter add-on module for Foundry VTT. It is not a dependancy of this module, but it is recommended. You can submit any issues via the Post Bug button available from the Game Settings tab within Foundry VTT.

**GitLab Issues:** You can also submit directly via GitLab. Please submit your issues here: https://gitlab.com/jaimechambers/the-digital-dungeon-master-5e-tools/-/issues
